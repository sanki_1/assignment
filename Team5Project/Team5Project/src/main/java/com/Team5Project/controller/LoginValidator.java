package com.Team5Project.controller;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.Team5Project.pojo.BloodDonor;
import com.Team5Project.pojo.Person;

@Component
public class LoginValidator implements Validator {

	 @Override
	    public boolean supports(Class aClass)
	    {
	        return aClass.equals(Person.class);
	    }
	    
	    @Override
	    public void validate(Object obj, Errors errors)
	    {
	    	Person userP = (Person) obj;
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "error.invalid.userP", "User Name Required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "error.invalid.userP", "Password Required");
	        
	    }
}
