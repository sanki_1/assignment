package com.day1;

import java.util.Scanner;

public class ArthimeticOperations {

	public static void main(String[] args) {
		int a,b,add,sub,mul;
		float div,rem;
		 Scanner sc = new Scanner(System.in);
		 System.out.print("Enter Two Numbers : ");
		    a = sc.nextInt();
		    b = sc.nextInt();
		    add=a+b;
		    sub=a-b;
		    mul=a*b;
		    div=(float)a/b;
		    rem=(float)a%b;
		    System.out.println("Sum = " + add);
		    System.out.println("Difference = " + sub);
		    System.out.println("Multiplication = " + mul);
		    System.out.println("Division = " + div);
		    System.out.println("remainder ="+rem);
	}

}
