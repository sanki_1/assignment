package com.day1;

public class Ascii6 {

	public static void main(String[] args) {
		int ch1 = 'a';  
		int ch2 = 'b';  
		int ch3 = 'A';
		System.out.println("The ASCII value of a is: "+ch1);  
		System.out.println("The ASCII value of b is: "+ch2);  
		System.out.println("The ASCII value of c is: "+ch3);  

	}

}
