package com.day1;

import java.util.Scanner;

class UserMain{
	public static int sumOfSquaresOfEvenDigits(int num){
		int rem;
		int sqsum=0;
		if (num>0){
			while (num>0){
				rem=num%10;
				num=num/10;
				if (rem%2==0){
					sqsum=sqsum+(rem*rem);
				}
			}
			if(sqsum==0){
				return 1;
			}
			else{
				return sqsum;
			}
		}
		else {
			return 0;
		}
	}

			}
public class Main9 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		
		int n=sc.nextInt();
		int r=UserMain.sumOfSquaresOfEvenDigits(n);
		if (r==0){
			System.out.println("Enter a positive number");
		}
		else if(r==1){
			System.out.println("No even digits. 0");
		}
		else{
			System.out.println(r);
			sc.close();
		}
	}



	}


