package com.day1;

import java.util.Scanner;

class UserMainCode{
	public static String getLongestWord(String s){
		String[] strarr=s.split(" ");
		String maxstr=strarr[0];
		for (int i=0;i<strarr.length;i++){
			if (strarr[i].length()>maxstr.length()){
				maxstr=strarr[i];
			}
		}
		return maxstr;
	}
}
public class Main10 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String str=sc.nextLine();
		System.out.println(UserMainCode.getLongestWord(str));
		sc.close();

	}

}
