1)	Design an HTML page with the following
solution:
<!DOCTYPE html>
<html>
<head>
<script >
function validate()
{ 
   if( document.StudentRegistration.name.value == "" )
   {
     alert( "Please provide your Name!" );
    
     return false;
   }
   if( document.StudentRegistration.fathername.value == "" )
   {
     alert( "Please provide your Father Name!" );
     
     return false;
   }
   
   if( document.StudentRegistration.postaladdress.value == "" )
   {
     alert( "Please provide your Postal Address!" );
    
     return false;
   }
   if( document.StudentRegistration.personaladdress.value == "" )
   {
     alert( "Please provide your Personal Address!" );
     
     return false;
   }
   if ( ( StudentRegistration.gender[0].checked == false ) && ( StudentRegistration.gender[1].checked == false ) &&(StudentRegistration.gender[2].checked == false))
   {
   alert ( "Please choose your Gender: Male or Female or other" );
   return false;
   }   
   if( document.StudentRegistration.City.value == "-1" )
   {
     alert( "Please provide your City!" );
    
     return false;
   }   
   if( document.StudentRegistration.Course.value == "-1" )
   {
     alert( "Please provide your Course!" );
    
     return false;
   }   
    
   if( document.StudentRegistration.State.value == "-1" )
   {
     alert( "Please provide your  State!" );
     
     return false;
   }
   if( document.StudentRegistration.pincode.value == "")
   {
   alert( "Please provide a pincode in given format" );
     
     return false;
   }
  if ( document.StudentRegistration.emailid.value== "" ) 
 {
     alert("Please enter correct email ID")
     
     return false;
 }
  if( document.StudentRegistration.dob.value == "" )
   {
     alert( "Please provide your DOB!" );
     
     return false;
   }
 
  if( document.StudentRegistration.mobileno.value == "")
  {
  alert( "Please provide a Mobile No in the format 12345....9." );
  return false;
}
   return( true );
}
</script>
</head>
<body>
<form  name="StudentRegistration" onsubmit="return(validate());">

<table cellpadding="2" width="20%" bgcolor="99FFFF" align="center"
cellspacing="2">
<tr>
<td colspan=2>
<center><font size=4><b>Student Registration Form</b></font></center>
</td>
</tr>
<tr>
<td>Name</td>
<td><input type=text name=name id="name" size="30"></td>
</tr>
<tr>
<td>Father Name</td>
<td><input type="text" name="fathername" id="fathername"
size="30"></td>
</tr>
<tr>
<td>Postal Address</td>
<td><input type="text" name="postaladdress" id="paddress" size="30"></td>
</tr>
<tr>
<td>Personal Address</td>
<td><input type="text" name="personaladdress"
id="personaladdress" size="30"></td>
</tr>
<tr>
<td>Gender</td>
<td>
<input type="radio" id="male" name="gender" value="male">
<label for="male">Male</label>
<input type="radio" id="female" name="gender" value="female">
<label for="female">Female</label>
<input type="radio" id="other" name="gender" value="other">
<label for="other">Other</label></td>
<tr>
<td>City</td>
<td><select name="City">
<option value="-1" selected>select</option>
<option value="New Delhi">NEW DELHI</option>
<option value="Banglore">BANGLORE</option>
<option value="Kolkata">KOLKATA</option>
<option value="Chennai">CHENNAI</option>
</select></td>
</tr>
<tr>
<td>Course</td>
<td><select name="Course">
<option value="-1" selected>select</option>
<option value="IT">IT</option>
<option value="CSE">CSE</option>
<option value="ECE">ECE</option>
<option value="BBA">BBA</option>
</select></td>
</tr>
<tr>
<td>State</td>
<td><select Name="State">
<option value="-1" selected>select</option>
<option value="AndhraPradesh">AP</option>
<option value="Assam">ASSAM</option>
<option value="Goa">GOA</option>
<option value="Punjab">PUNJAB</option>
</select></td>
</tr>
<tr>
<td>PinCode</td>
<td><input type="text" name="pincode" id="pincode" size="20" pattern=[0-9]{6} ></td>
</tr>
<tr>
<td>EmailId</td>
<td><input type="text" name="emailid" id="emailid" size="20"></td> 
</tr>
<tr>
<td>DOB</td>
<td><input type="date" name="dob"/></td>
</tr>
<tr>
<td>MobileNo</td>
<td><input type="tel" name="mobileno" pattern=[0-9]{10} /></td>
</tr>
<tr>
<td><input type="reset"></td>
<td colspan="2"><input type="submit" value="Submit Form" /></td>
</tr>
</table>
</form>
</body>
</html>