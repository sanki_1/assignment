package com.car;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class car_crudoperations {
	    private String carId;
		private String make;
		private String model;
		private int year;
		private double price;
		public car_crudoperations()
		{
			
		}
		public car_crudoperations(String carId, String make, String model, int year, double price) {
			super();
			this.carId = carId;
			this.make = make;
			this.model = model;
			this.year = year;
			this.price = price;
		}
		public String getCarId() {
			return carId;
		}
		public void setCarId(String carId) {
			this.carId = carId;
		}
		public String getMake() {
			return make;
		}
		public void setMake(String make) {
			this.make = make;
		}
		public String getModel() {
			return model;
		}
		public void setModel(String model) {
			this.model = model;
		}
		public int getYear() {
			return year;
		}
		public void setYear(int year) {
			this.year = year;
		}
		public double getPrice() {
			return price;
		}
		public void setPrice(double price) {
			this.price = price;
		}
		@Override
		public String toString() {
			String str=String.format("%-15s %-15s %-15s %-15s $%-7s", carId,year,make,model,price);
			return str;
					}
		public int carCount() throws ClassNotFoundException {
			int num=0;
			try {
				Connection con=DBConnection.getConnection();
				Statement st=con.createStatement();
				ResultSet rs=st.executeQuery("select count(carID) from car");//To count the no of cars in the table 
				while (rs.next())
					num=rs.getInt(1);
				con.close();
				return num;
			}catch(SQLException e) {
				e.printStackTrace();
			}
			return num;
		}
		
		public List<car_crudoperations> carDisplay() throws ClassNotFoundException, SQLException {
			List<car_crudoperations> lc=new ArrayList<car_crudoperations>();
			car_crudoperations c;
			Connection con=DBConnection.getConnection();
			try {
			Statement st=con.createStatement();
			ResultSet rs=st.executeQuery("select * from car;");//To display the list of cars
			while (rs.next()) {
				c=new car_crudoperations(rs.getString(1),rs.getString(2),rs.getString(3),rs.getInt(4),rs.getDouble(5));
			    lc.add(c);
			}}catch(SQLException e) {
				e.printStackTrace();
			}
			return lc;	
		}
		public void carAdd(String carID,String make,String model,int year,double price) throws ClassNotFoundException {
		    int res=0;
			try{Connection con=DBConnection.getConnection();
			Statement st=con.createStatement();
			res=st.executeUpdate("insert into car values('"+carID+"','"+make+"','"+model+"',"+year+","+price+")");//To insert the values into car table
			//res is used to store the integer value returned by executeupdate query
			con.close();
			}catch(SQLIntegrityConstraintViolationException sqie) {
				System.out.println("The car you are trying to enter is already present");
			}catch(SQLException e) {
				e.printStackTrace();
			}
			if (res==1) {
				System.out.println("car is Added successfully");
				}
			else {
				System.out.println("car could not added");
				}
			
}
			
			
		
		public void carDelete(String carID) throws ClassNotFoundException {
			int num=0;
			int res=0;
			try{Connection con=DBConnection.getConnection();
			Statement st=con.createStatement();
			res=st.executeUpdate("delete from car where carID='"+carID+"'");//To delete the row when the carid is given
			}catch(SQLException se) {
				se.printStackTrace();
			}
			if (res==1)
				System.out.println("Deleted successfully from the car table");
			else
				System.out.println("Could not delete the car from the car table");
			
		}
		public void carUpdateMake(String carID,String make) throws ClassNotFoundException {
			
			int makeres=0;
			try{Connection con=DBConnection.getConnection();
			Statement st=con.createStatement();
			makeres=st.executeUpdate("update car set carMake='"+make+"' where carID='"+carID+"'");//To update the make of the car when carid is given 
			}catch(SQLException e) {
				e.printStackTrace();
			}
			if (makeres==1)
				System.out.println("Make column is Updated successfully");
			else
				System.out.println("Make column is not Updated successfully");
			
		}
		public void carUpdateModel(String carID,String model) throws ClassNotFoundException {
			
			int modelres=0;
			try{Connection con=DBConnection.getConnection();
			Statement st=con.createStatement();
			modelres=st.executeUpdate("update car set carModel='"+model+"' where carID='"+carID+"'");//To update the model of the car when carid is given 
			}catch(SQLException e) {
				e.printStackTrace();
			}
			if (modelres==1)
				System.out.println("Model column is Updated successfully");
			else
				System.out.println("Model column is not Updated successfully");
		}
		public void carUpdateYear(String carID,int year) throws ClassNotFoundException {
			
			int yearres=0;
			try{Connection con=DBConnection.getConnection();
			Statement st=con.createStatement();
		    yearres=st.executeUpdate("update car set year='"+year+"' where carID='"+carID+"'");//To update the year of the car when carid is given 
			}catch(SQLException se) {
				se.printStackTrace();
			}
			if (yearres==1)
				System.out.println("Year column is Updated successfully");
			else
				System.out.println("Year column is not Updated successfully");
		}
		public void carUpdatePrice(String carID,double price) throws ClassNotFoundException {
			
			int priceres=0;
			try{Connection con=DBConnection.getConnection();
			Statement st=con.createStatement();
			priceres=st.executeUpdate("update car set price='"+price+"' where carID='"+carID+"'");//To update the price of the car when carid is given 
			}catch(SQLException e) {
				e.printStackTrace();
			}
			if (priceres==1)
				System.out.println("price column is Updated successfully");
			else
				System.out.println("price column is Updated successfully");}
		}
		
   



