package com.car;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class CarInventorySystem {
    static void menu()
    {
    	System.out.println("1.add");
    	System.out.println("2.list");
    	System.out.println("3.delete");
    	System.out.println("4.update");
    	System.out.println("5.quit");
    }
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		System.out.println(String.format("welcome to joe gently used autos"));
        String choice;
        Scanner sc=new Scanner(System.in);
        car_crudoperations c=new car_crudoperations();
        do 
        {
        	menu();
        	System.out.println("Enter command:");
        	choice=sc.nextLine();
        	switch(choice)
        	{
        	case "add" :
        		try {
        			int noOfCars=c.carCount();
        			if (noOfCars<20) {
        			System.out.print("Car ID: ");
        			String carID=sc.nextLine();
        			System.out.print("Make: ");
        			String make=sc.nextLine();
        			System.out.print("Model: ");
        			String model=sc.nextLine();
        			System.out.print("Year: ");
        			int year=sc.nextInt();
        			System.out.print("Price($): ");
        			double price=sc.nextDouble();
        			c.carAdd(carID, make, model, year, price);
        			
        			
        			}
        			else {
        				System.out.println("it has reached max limit please delete some entries to enter");
        				break;
        			}
        			sc.nextLine();
        			}catch(Exception e) {
        				System.out.println("Enter proper input");
        				sc.nextLine();
        			}
    			break;
			case "list" :
				List<car_crudoperations> carList=c.carDisplay();
        		int noofcars=c.carCount();
        		
        		System.out.println(String.format("%-15s %-15s %-15s %-15s $%-10s", "Car ID","Year","Maker","Model","Price"));
    			for (car_crudoperations cars:carList) {
    				System.out.println(cars);
    			}
    			System.out.println("number of cars:"+noofcars);
    			double total=carList.stream().map(p->p.getPrice()).collect(Collectors.summingDouble(Double::doubleValue));
    			System.out.println("\nTotal inventory :$"+total+"\n");
        		break;
        	case "delete":
        		System.out.println("Enter the ID of the car you need to delete");
    			String carID=sc.nextLine();
    			c.carDelete(carID);
    			
        		break;
        	case "update" :
        		System.out.println("Enter the ID of the car that need to be update");
    			String car_Id=sc.nextLine();
    			System.out.println("Enter the coulmn you want to update");
    			String columnChoice=sc.nextLine();
    			switch(columnChoice) {
    			case "make":
    				System.out.println("Enter the  make that you need to update");
    				String make=sc.nextLine();
    				c.carUpdateMake(car_Id, make);
    				
    			break;
    			case "model":
    				System.out.println("Enter the model that need to update");
    				String model=sc.nextLine();
    				c.carUpdateModel(car_Id, model);
    				
    			break;
    			case "year":
    				try {
    				System.out.println("Enter the year that need to update");
    				int year=Integer.parseInt(sc.nextLine());
    				c.carUpdateYear(car_Id, year);
    				
    				}catch(InputMismatchException e) {
    					System.out.println("Enter integer input properly");
    				}
    			break;
    			case "price":
    				try {
    				System.out.println("Enter the price that need to update");
    				double price=Double.parseDouble(sc.nextLine());
    				c.carUpdatePrice(car_Id, price);
    				}
    				catch(InputMismatchException e) {
    					System.out.println("Enter double input properly");
    				
    				}
    			default:
    				System.out.println("Enter a valid column");
    				
    			}
        		break;
    			
        	case "quit" :
        		System.out.println("GOOD BYE!");
        		break;
        	default:
        		System.out.println("Sorry but \""+choice+"\" is not a valid command. Please try again.");
    				break;	
    			}
        	}while(choice.charAt(0)!='\0');
        sc.close();
    			
        	}
	}




