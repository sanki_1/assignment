package com.day5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class CheckedException {

	public static void main(String[] args) {
		String filename = "C:\\1\\checkedexception.txt";
		File f = new File(filename);
		try {
		FileReader fr = new FileReader(f);
		} 
		catch (FileNotFoundException e) {				
		System.out.println(e);
		}
	}

}
