package com.day5;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class RunRate
{
	
	RunRate(int total_runs_scored, int total_overs_faced)
	{
		try {
			float Run_rate = total_runs_scored/total_overs_faced;
			System.out.println("Current Run Rate :"+Run_rate);
		}
		catch (Exception e)
		{
			System.out.println("java.lang.ArithmeticException");
		}
	}
}
public class RunrateException {

	public static void main(String[] args) throws Exception {
		BufferedReader reader =new BufferedReader(new InputStreamReader(System.in));
		   
	      System.out.println("Enter the total runs scored ");
	      int runs = Integer.parseInt(reader.readLine());
	      System.out.println("Enter the total overs faced");
	      int overs = Integer.parseInt(reader.readLine());
	      
	      RunRate rr=new RunRate(runs,overs);

	}

}
