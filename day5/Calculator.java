package com.day5;
class MyCalculator {
	
	void power(int n, int p) 
	{
			if(n>0&&p>0)
			{
				int result= (int) Math.pow(n, p);
				System.out.println(result);
			}		
			if(n==0&&p==0)
			{
				try {
					throw new Exception("n and p should not be zero.");
				} catch (Exception e) {
					
					e.printStackTrace();
				}
			}
			 if((n<0&&p<0))
			{
				 try {
						throw new Exception("n or p should not be negative.");
					} catch (Exception e) {
						
						e.printStackTrace();
					}
				}
				 if((n<0&&p>0))
					{
						try {
							throw new Exception("n or p should not be negative.");
						} catch (Exception e) {
							
							e.printStackTrace();
						}
					}
		}
		  
}	 
			
public class Calculator {

	public static void main(String[] args) {
MyCalculator cal=new MyCalculator();
		
		cal.power(3,5);
		
		cal.power(2,4);
		
		cal.power(0,0);
		
		cal.power(-1,-2);
		
		cal.power(-1,2);

	}

}
