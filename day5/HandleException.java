package com.day5;
import java.util.Scanner;

class ExceptionHandling
{
	ExceptionHandling(int FirInt,int SecInt) {
		try {		
			int c=FirInt/SecInt;
			System.out.println(c);
		}
	    catch (Exception e) {
	    	System.out.println(e);
	    }
	}
}
public class HandleException {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int FirInt = sc.nextInt();
		int SecInt = sc.nextInt();
		
		ExceptionHandling eh=new ExceptionHandling(FirInt, SecInt);
	}

}
