package com.day5;

import java.util.Scanner;

class CustomException extends Exception
{
CustomException(String s)
{
super(s);
}
}
public class CustomExceptionDemo {

	public static void main(String[] args) throws CustomException {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the Player name:");
		String name=sc.nextLine();
		System.out.println("Enter the Player age :");
		int age=sc.nextInt();
		if(age<19)
		{
		throw new CustomException("InvalidAgeRangeException");
		}
		else
		{
			System.out.println("Player name :"+name);
			System.out.println("Player age :"+age);
		}

}

}
