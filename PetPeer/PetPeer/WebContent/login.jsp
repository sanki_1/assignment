<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="styles.css">
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="script.js"></script>
<title>Login</title>
</head>
<body>
<%
	if(session.getAttribute("user")!=null){
		response.sendRedirect("/PetDetailsServlet");
	}
%>
<div id='cssmenu'>
<ul>
   <li><a href='#'><span>PetPeers</span></a></li>
   <li><a href='PetDetailsServlet'><span>Home</span></a></li>
   <li><a href='useregn.jsp'><span>User Registration</span></a></li>
   <li class='active'><a href='login.jsp'><span>Login</span></a></li>
</ul>
</div>
<br><br><br>
<br><br><br><br><br>
<div >
<form  id="login" action="MainsServlet" method="post" style="text-align: center;"><br>
<input type="hidden" name="jspPage" value="login.jsp" />
<b>User Name:</b><input type="text" name="userName" required="required"><br><br>
<b>Password :</b><input type="password" name="password" required="required"><br><br>
<input type="submit" name="loginButton" value="Login">   <button type="reset" name="reset" value="Reset">Reset</button> 
</form>
</div>
</body>
</html>