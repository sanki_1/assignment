<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import = "java.io.*,java.util.*,com.model.Pet" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="styles.css">
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script src="script.js"></script>
<title>My Pets</title>

</head>
<body>
<%
	//allow access only if session exists
	if(session.getAttribute("user")==null){
		response.sendRedirect("login.jsp");
	}
%>
<div id='cssmenu'>
<ul>
   <li><a href='PetDetailsServlet'><span>Home</span></a></li>
   <li class='active'><a href='MyPetsServlet'><span>My Pets</span></a></li>
   <li><a href='pet_form.jsp'><span>Add Pet</span></a></li>
   <li><a href='logout.jsp'><span>Logout</span></a></li>
</ul>
</div>
<center>
<h2>Pet List</h2>
        
        <table id="table1" border="1" width="500" height="200">
			<tr>
				<td width="119" align="center"><font size="5sp"><b>Id</b></font></td>
				<td width="168" align="center" ><font size="5sp"><b>PetName</b></font></td>
				<td width="168" align="center"><font size="5sp"><b>Place</b></font></td>
				<td width="119" align="center"><font size="5sp"><b>Age</b></font></td>
			</tr>
		<% 
		List<Pet> petList= (List<Pet>)request.getAttribute("pet");
		for (int i=0;i<petList.size();i++)
		{
		%>
		<tr>
			<td width="119" align="center"><font size="4sp"><%=petList.get(i).getPetId()%></font></td>
			<td width="168" align="center"><font size="4sp"><%=petList.get(i).getPetName()%></font></td>
			<td width="168" align="center"><font size="4sp"><%=petList.get(i).getPetPlace()%></font></td>
			<td width="119" align="center"><font size="4sp"><%=petList.get(i).getPetAge()%></font></td>
		</tr>
		<%}%>
		</table>
</center>
</body>
</html>