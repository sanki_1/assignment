package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.model.Pet;
import com.model.User;

public class LoginDao implements DaoInterface{
	
	public boolean create(User user) {
		return false;
	}

	
	public boolean read(User user) {
		return false;
	}

	
	public boolean update(User user, int petId) {
		return false;
	}

	
	public boolean delete(String userId) {
		return false;
	}

	
	public String readLogin(User user) {
		String userName=" ";
		PreparedStatement preparedStatement = null;
		String query="";
		ResultSet resultSet=null;
		Connection connection=DBConnection.getConnection();
		try {
			query="SELECT USER_NAME FROM PET_USER WHERE USER_NAME=? AND USER_PASSWD=?";
			preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1, user.getUserName());
			preparedStatement.setString(2, user.getPassword());
			resultSet=preparedStatement.executeQuery();
			if(!resultSet.next()){
				userName=" ";
			}
			else{
				userName=resultSet.getString("USER_NAME");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userName;
	}

	
	public boolean savePet(Pet pet) {
		
		return false;
	}

	
	public List<Pet> readPet() {
		
		return null;
	}

	
	public List<Pet> readPet(User user) {
		
		return null;
	}
	
}
