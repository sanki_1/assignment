package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.model.Pet;
import com.model.User;

public class SavePetDao implements DaoInterface {
	
	public boolean savePet(Pet pet) {
		PreparedStatement preparedStatement = null;
		String query="";
		Connection connection=DBConnection.getConnection();
		try {
			query="INSERT INTO PETS(PET_NAME,PET_AGE,PET_PLACE) VALUES(?,?,?)";
			preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1, pet.getPetName());
			preparedStatement.setInt(2, pet.getPetAge());
			preparedStatement.setString(3, pet.getPetPlace());
			preparedStatement.execute();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	
	public boolean create(User user) {
		
		return false;
	}

	
	public boolean read(User user) {
		
		return false;
	}

	
	public boolean update(User user, int petId) {
		
		return false;
	}

	
	public boolean delete(String userId) {
		
		return false;
	}

	
	public String readLogin(User user) {
		
		return null;
	}

	
	public List<Pet> readPet() {
		
		return null;
	}

	
	public List<Pet> readPet(User user) {
		
		return null;
	}

}

