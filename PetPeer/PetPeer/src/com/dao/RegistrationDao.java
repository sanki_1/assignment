package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.model.Pet;
import com.model.User;

public class RegistrationDao implements DaoInterface {
	
	public boolean create(User user) {
		PreparedStatement preparedStatement = null;
		String query="";
		Connection connection=DBConnection.getConnection();
		try {
			query="INSERT INTO PET_USER(USER_NAME,USER_PASSWD) VALUES(?,?)";
			preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1, user.getUserName());
			preparedStatement.setString(2, user.getPassword());
			if(preparedStatement.execute()){
				return true;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	
	public boolean read(User user) {
		// TODO Auto-generated method stub
		return false;
	}


	public boolean update(User user, int petId) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public boolean delete(String userId) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String readLogin(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean savePet(Pet pet) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Pet> readPet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pet> readPet(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
