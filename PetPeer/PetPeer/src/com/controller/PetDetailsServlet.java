package com.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.HomeDao;
import com.model.Pet;


@WebServlet("/PetDetailsServlet")
public class PetDetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		HomeDao homeDao=new HomeDao();
		List<Pet> petList=homeDao.readPet();
		request.setAttribute("pet",petList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("pet_home.jsp"); 
		dispatcher.forward(request, response);
	}

	

}
