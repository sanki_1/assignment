Joins
1.Write a query in SQL to display the first name, last name, department number, and department name for each employee
query:
mysql> SELECT E.first_name,E.last_name,D.department_id, D.department_name FROM employees E JOIN departments D  ON E.department_id = D.department_id;
+------------+-----------+---------------+-----------------+
| first_name | last_name | department_id | department_name |
+------------+-----------+---------------+-----------------+
| valli      | patabal   |            60 | it              |
| david      | austin    |            60 | it              |
| bruce      | ernst     |            60 | it              |
| alexander  | hunold    |            60 | it              |
| lex        | dehaan    |            90 | executive       |
| neena      | kochaaar  |            90 | executive       |
| steven     | king      |            90 | executive       |
| william    | gietz     |           110 | accounting      |
| shelley    | higgins   |           110 | accounting      |
| diana      | lorentz   |           110 | accounting      |
+------------+-----------+---------------+-----------------+
10 rows in set (0.08 sec)
2.Write a query in SQL to display the first name, last name, department number and department name, for all employees for departments 80 or 40
query:
mysql> SELECT E.first_name,E.last_name,D.department_id, D.department_name FROM employees E JOIN departments D  ON E.department_id = D.department_id and e.department_id in(80,40);
Empty set (0.05 sec)
3.Write a query in SQL to display the first name of all employees including the first name of their manager
query:
mysql> select e.first_name as "Employee_name",m.first_name as "Manager_name" from employees e join departments d on (e.department_id=d.department_id) join employees m on(m.employee_id=d.manager_id);
+---------------+--------------+
| Employee_name | Manager_name |
+---------------+--------------+
| steven        | steven       |
| neena         | steven       |
| lex           | steven       |
| alexander     | alexander    |
| bruce         | alexander    |
| david         | alexander    |
| valli         | alexander    |
| diana         | shelley      |
| shelley       | shelley      |
| william       | shelley      |
+---------------+--------------+
10 rows in set (0.04 sec)
4.Write a query in SQL to display all departments including those where does not have any employee
query:
mysql> select d.department_name FROM employees e RIGHT OUTER JOIN departments d ON e.department_id = d.department_id;
+------------------+
| department_name  |
+------------------+
| administration   |
| marketing        |
| purchasing       |
| human resources  |
| shipping         |
| it               |
| it               |
| it               |
| it               |
| public relations |
| sales            |
| executive        |
| executive        |
| executive        |
| finance          |
| accounting       |
| accounting       |
| accounting       |
+------------------+
18 rows in set (0.04 sec)
5.Write a query in SQL to display the first name, last name, department number and name, for all employees who have or have not any department
query:
mysql> SELECT E.first_name, E.last_name, E.department_id, D.department_name FROM employees E LEFT OUTER JOIN departments D ON E.department_id = D.department_id;
+------------+-----------+---------------+-----------------+
| first_name | last_name | department_id | department_name |
+------------+-----------+---------------+-----------------+
| steven     | king      |            90 | executive       |
| neena      | kochaaar  |            90 | executive       |
| lex        | dehaan    |            90 | executive       |
| alexander  | hunold    |            60 | it              |
| bruce      | ernst     |            60 | it              |
| david      | austin    |            60 | it              |
| valli      | patabal   |            60 | it              |
| diana      | lorentz   |           110 | accounting      |
| shelley    | higgins   |           110 | accounting      |
| william    | gietz     |           110 | accounting      |
+------------+-----------+---------------+-----------------+
10 rows in set (0.00 sec)
Jdbc Exercise(Using Statement and PreparedStatement)
1.Let’s write code to insert a new record into the table Users with following details:
o	username: steve
o	password: secretpass
o	fullname: steve paul
o	email: steve.paul@hcl.com
package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class insert {

	public static void main(String[] args) throws SQLException {
		Connection con=null;
		Statement st=null;
		try
		{

		Class.forName("com.mysql.cj.jdbc.Driver");//
		//DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","Sanki@");
		st=con.createStatement();
		st.execute("insert into users values('steve','secretpass','steve paul','steve.paul@hcl.com')");
		
		}
		catch(Exception e)
		{
		System.out.println(e);
		}
		finally
		{
		st.close();
		con.close();
		}

	}

}

mysql> select * from users;
+----------+------------+------------+--------------------+
| username | password   | fullname   | email              |
+----------+------------+------------+--------------------+
| steve    | secretpass | steve paul | steve.paul@hcl.com |
+----------+------------+------------+--------------------+
1 row in set (0.00 sec)

2. Select all records from the Users table and print out details for each record.
package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class select{

	public static void main(String[] args) throws SQLException {
		Connection con=null;
		Statement st=null;
		ResultSet rs = null;
		try
		{

		Class.forName("com.mysql.cj.jdbc.Driver");
		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","Sanki123");
		st=con.createStatement();
		rs=st.executeQuery("select * from users");
		while (rs.next()) {
			
			System.out.println(rs.getString(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4));

		}
		}
		catch(Exception e)
		{
		System.out.println(e);
		}
		finally
		{
		st.close();
		con.close();
		}

	}

}
output:
steve secretpass steve paul steve.paul@hcl.com
sankeerthana sanki sanky sanky@hcl.com


3. Write a code to update all the details  of “steve paul”.
package com.jdbc;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class update {

	public static void main(String[] args) throws SQLException {
		Connection con=null;
		Statement st=null;
		try
		{

		Class.forName("com.mysql.cj.jdbc.Driver");//
		//DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","Sanki@");
		st=con.createStatement();
		st.executeUpdate("update users set password='pass',email='paul@hcl.com' where fullname='steve paul'");
		}
		catch(Exception e)
		{
		System.out.println(e);
		}
		finally
		{
		st.close();
		con.close();
		}

	}

};
output:
mysql> select * from users;
+--------------+----------+------------+---------------+
| username     | password | fullname   | email         |
+--------------+----------+------------+---------------+
| steve        | pass     | steve paul | paul@hcl.com  |
| sankeerthana | sanki    | sanky      | sanky@hcl.com |
+--------------+----------+------------+---------------+
2 rows in set (0.04 sec)

4.Write a code to delete a record whose username field contains “steve”.
package com.jdbc;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class delete{

	public static void main(String[] args) throws SQLException {
		Connection con=null;
		Statement st=null;
		try
		{

		Class.forName("com.mysql.cj.jdbc.Driver");//
		//DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","Sanki@");
		st=con.createStatement();
		st.executeUpdate("delete from users where username='steve'");
		}
		catch(Exception e)
		{
		System.out.println(e);
		}
		finally
		{
		st.close();
		con.close();
		}

	}

};
output:
mysql> select * from users;
+--------------+----------+----------+---------------+
| username     | password | fullname | email         |
+--------------+----------+----------+---------------+
| sankeerthana | sanki    | sanky    | sanky@hcl.com |
+--------------+----------+----------+---------------+
1 row in set (0.02 sec)
		


Jdbc Transaction Management
1.Create a table named customer including name,salary,email.
package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class create{

	public static void main(String[] args) throws SQLException {
		Connection con=null;
		Statement st=null;
		try
		{

		Class.forName("com.mysql.cj.jdbc.Driver");//
		//DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","Sanki@");
		st=con.createStatement();
		st.execute("create table customer(name varchar(30),salary varchar(30),email varchar(40) unique) ");
		
		}
		catch(Exception e)
		{
		System.out.println(e);
		}
		finally
		{
		st.close();
		con.close();
		}

	}

}
2.One of the constraints on the table is that email has to be unique. If we enter same email a second time to violate this constraint. It results in SQL exception.
 Have to  rollback transaction programmatically in exception handling block.
package com.jdbc;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class Transaction {

	public static void main(String[] args) throws SQLException {
		Connection con = null;

		try {
			while (true) {

				Scanner sc = new Scanner(System.in);
				System.out.println("Enter name:");
				String name = sc.next();
				System.out.println("Enter Salary:");
				int salary = sc.nextInt();
				System.out.println("Enter email:");
				String email = sc.next();
				Class.forName("com.mysql.cj.jdbc.Driver");

				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/practice", "root", "Sanki@");
				con.setAutoCommit(false);

				PreparedStatement ps = con.prepareStatement("insert into customer values(?,?,?)");
				ps.setString(1, name);
				ps.setInt(2, salary);
				ps.setString(3, email);
				ps.executeUpdate();
				con.commit();
				System.out.println("Want to add more records y/n");
				String r = sc.next();
				if (!"y".equalsIgnoreCase(r)) {
					System.out.println("Data saved successfully");
					break;
				}
			}

		} catch (Exception e) {

			con.rollback();
			System.out.println("Email Id must be unique . Data Rollback successfully");

		} finally {
			con.close();
		}

	}

}
