package com.day_2;

import java.util.Scanner;

public class Main2 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        System.out.print("Input a string: ");
        String str = in.nextLine();
        System.out.print("The middle character in the string: " + middlechar(str)+"\n");
    }
 public static String middlechar(String str)
    {
        int pos;
        int length;
        if (str.length() % 2 == 0)
        {
            pos = str.length() / 2 - 1;
            length= 2;
        }
        else
        {
            pos = str.length() / 2;
            length = 1;
        }
        return str.substring(pos, pos + length);
	}

}
