package com.day_2;
class A
{
int i, j;
void showij()
{
System.out.println ("i and j: " + i +" "+ j );
}
}
class B extends A
{
int k;
void showk()
{
System.out.println ( "K: " + k); 
}
void sum()
 { 
	System.out.println ("i + j + k: " + ( i + j + k ) );
 }
} 
 
public class Main5 {

	public static void main(String[] args) {
		A a = new A();
		 B b = new B();

		 // the superclass may be used by itself
		 a.i = 10;
		 a.j = 20;
		 
		 a.showij() ;
		 
		
		 b. i = 1;
		 b. j = 2;
		 b. k = 3;
		 
		 b.showij ( ) ;
		 b.showk ( );
		
		 b.sum ( );
		 }
	}


